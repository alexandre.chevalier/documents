### Question A1.1
> Donner la raison pour laquelle le système de gestion de base de données (SGBD) ne parvient pas à réaliser le dernier ajout.
### Réponse
Le membre id=312 a déjà un instrument principal enregistré, or chaque membre ne doit déclarer qu'un instrument principal au sein de son groupe. La dernière requête essaye d'ajouter un deuxième instrument pour le membre 312 d'où l'erreur.

### Question A2.1
> Proposer le code de la fonction stockée `getDureeExistenceUnGroupe(paramIdGroupe int)` qui renvoie le nombre d’années d’existence du groupe dont l’identifiant est passé en paramètre.
### Réponse
```sql
FUNCTION getDureeExistenceUnGroupe(paramIdGroupe int) returns int
begin
  return (
    select TIMESTAMPDIFF(YEAR, dateCreation, CURDATE())
    from Groupe where id = paramIdGroupe
  );
end
```

### Question A2.2
> Écrire les requêtes répondant aux besoins exprimés ci-dessus.
### Réponses
> a) la liste des membres (prénom, nom et instrument principal du membre, nom de son groupe), triée par ordre alphabétique croissant sur le nom du groupe ;
```sql
SELECT nom, prenom, Instrument.libelle
FROM Membre
JOIN Jouer_Groupe ON Membre.id = Jouer_Groupe.idMembre
JOIN Instrument ON Jouer_Groupe.idInstrument = Instrument.id
ORDER BY nom ASC;
```
> b) pour chaque groupe, son nom et son nombre de membres.
```sql
SELECT Groupe.nom, COUNT(Membre.id) AS "Nombre de membres"
FROM Membre
JOIN Groupe ON Groupe.id = Membre.idGroupe;
```

### Question B1.1
> Proposer une modélisation de la structure de la base de données pour prendre en compte les informations sur la gestion spécifique des étapes du planning d’accompagnement. Seuls les éléments du schéma existant concernés par l’évolution seront repris dans le schéma proposé.
### Réponse

![Schéma](./bts-sio-sting-b-1-1.drawio.svg)

### Question C1.1
> Donner les deux commentaires attendus dans la méthode `GetAlbumsUnGroupe()` de la classe `FabrikZik`.
### Réponse
```csharp
// Lignes 7 et 8
// Récupération des albums en XML via une API dans une liste de noeuds XML.
// C'est le début de la désérialisation (XML vers IEnumerable de XElement).

// Lignes 10 à 18
// Extraction des informations de chaque noeud XML album.
// Instanciation d'Album à partir des informations extraites
// et ajout de chaque objet album à la liste résultat.
```

### Question C1.2
> Expliquer l’origine de la levée d’exception visible sur la copie d’écran et proposer une modification de code corrigeant la méthode.
### Réponse
Le noeud "url" n'existe pas dans le XML d'un album. Il fallait plutôt écrire "link" à la place de "url".

### Question C2.1
> Compléter le code de la méthode `GetTitresUnAlbum()`.
### Réponse
```csharp
public static List<Titre> GetTitresUnAlbum(Album unAlbum) {
  List<Titre> lesTitres = new List<Titre>();
  string urlDetailsTracksAlbum = "https://api.zikoc.com/2.0/album/" + unAlbum.getId().ToString()+"/tracks&output=xml";
  IEnumerable<XElement> xLesTracks = XDocument.Load(urlDetailsTracksAlbum).Root.Element("data").Elements("track");
  foreach (XElement xUnTrack in xLesTracks)
  {
    // à compléter sur votre copie (mission C2)
    int id = (int)xUnTrack.Element("id");
    string nom = xUnTrack.Element("title").Value;
    string urlPageWeb = xUnTrack.Element("link").Value;
    int duree = (int)xUnTrack.Element("duration");
    string extraitAudio = xUnTrack.Element("preview").Value;
    Titre titre = new Titre(id, nom, urlPageWeb, duree, extraitAudio, unAlbum);
    lesTitres.Add(titre);
  }
  return lesTitres;
}
```

### Question C3.1
> Répondre au courriel du chef de projet.
### Réponse
```pre
Bonjour,

Une API permet de cacher les détails (complexité) des implémentations aux clients de l'API.
Elle a aussi les avantages suivants :
- Rendre les évolutions plus simples
- Rendre le projet plus maintenable sur le long terme

Les inconvénients sont des développements et des tests supplémentaires à réaliser.
Car on rajoute une couche logicielle intermédiaire en plus.

Cordialement,
<Signature>
```

### Question D1.1
> Écrire le code de la méthode `ToString()` de la classe `Festival`.
```csharp
  // Exemple à retourner 
  // "Festival Occitanie en musique édition 2022 : 18 groupes sur 3 jours !"
  return nom + " édition " + annee + " : " 
    + laProgrammation.Count + "groupes sur " + lesDates.Count + " !";
```

### Question D1.2
> Écrire le code de la méthode `GroupesUneDate()` de la classe `Festival`.
### Réponse
```csharp
  List<Groupe> resultat = new List<Groupe>();
  foreach(Groupe groupe in laProgrammation.Keys) {
    // Si les deux dates sont égales
    if (laProgrammation[groupe].Compare(laDate) == 0)
    {
      resultat.Add(groupe);
    }
  }
  return resultat;
```

### Question D1.3
> Écrire le code de la méthode `GroupesParGenre()` de la classe `Festival`.
### Réponse
```csharp
  Dictionary<string, int> resultat = new Dictionary<string, int>();
  foreach(Groupe groupe in laProgrammation.Keys) {
    string genre = groupe.GetGenreMusical();
    if (resultat.ContainsKey(genre)) {
      // Si la clé est déjà là, on compte un groupe de plus
      resultat[genre] = resultat[genre] + 1;
    } else {
      // Sinon on initialise le genre avec un premier groupe
      resultat.Add(genre, 1);
    } 
  }
  return resultat;
```


### Question D2.1
> Expliquer ce résultat, en précisant ce que teste la méthode `ProgrammerGroupeHorsCalendrier()`.
### Réponse
Le test veut vérifier qu'un groupe ajouté (programmé) dans un festival, hors des dates prévues pour le festival, ne l'ajoute pas vraiment à la programmation (dans l'objet `laProgrammation`). Mais ce n'est pas le cas d'après l'implémentation de la méthode `ProgrammerGroupe` alors ce test va planter.

### Question D2.2
> Coder la modification de la méthode `ProgrammerGroupe()` de la classe `Festival` pour que le test précédent puisse réussir.
### Réponse
```csharp
public void ProgrammerGroupe(Groupe leGroupe, DateTime laDate)
{
  // Vérifier que la date est prévue avant d'ajouter
  if (lesDates.Contains(laDate)) {
    laProgrammation.Add(leGroupe,laDate);
  }
}
```
