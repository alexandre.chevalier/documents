# Correction du sujet 2020

## Question 1.1
Déduire de la requête SQL fournie dans le dossier documentaire, la règle de gestion utilisée par Coopain pour choisir une paillette lors d'une demande d'insémination.

**Réponse** :
La règle de gestion pour choisir la paillette lors d’une demande d’insémination est :
- Récupérer le lot de paillettes enrichies (type 3) le plus ancien (date minimum) correspondant au taureau dont l’id est fourni.

## Question 1.2
Indiquer pour chaque information à imprimer sur une paillette, la ou les données nécessaires en précisant pour chacune d'où elle provient dans le schéma existant et si elle nécessite une transformation.

**Réponse** :

### Date de collecte :
- Provenance : table Collecte, champ date
- On suppose que son format c’est AAAA-MM-JJ (type DATE)
    - Elle nécessite une transformation :
    - On doit la transformer en AAJJJ donc calculer le nombre de jours écoulés depuis le début de l’année jusqu’à la « date »
        - Calculer le nombre de jours entre le 1er janvier et « date »
        - Rajouter les deux derniers nombres de l’année devant

### N° d’agrément du CCS :
- Provenance : administration de Coopain ou dans une constante en fichier de configuration
- On ne sait pas donc non pas de transformation

### N° nation d’identification du taureau :
- Provenance : table Taureau, champ idNational
- Transformation : non

### Code barre : 
- N°IE du taureau : 
    - Provenance : table Taureau, champ numeroIE
    - Transformation : non
- Date de collecte exprimée en quantième :
    - Provenance : voir date de collecte plus haut
    - Transformation : oui car format AAJJJ
- Numéro de lot :
    - Provenance : table Lot, champ numero
Transformation : oui, il faut agréger les 3 informations et les transformer en code-barres

### 3 champs d’après : voir ceux du code-barres

### Code race : 
- Provenance : table Race, champ code
- Transformation : non

### Nom du taureau : 
- Provenance : table Taureau, champ nom
- Transformation : non

## Question 1.3
Écrire la requête permettant d'obtenir pour le taureau FR0103015562 les tarifs des types de paillette (libellé du type de paillette, prix de la paillette).

**Réponse** :
```sql
SELECT TypePaillette.libelle, Tarif.prixPaillette
FROM Tarif JOIN TypePaillette 
ON TypePaillette.id = idTypePaillette 
WHERE idNationalTaureau = "FR0103015562"
```

## Question 1.4
Écrire la requête permettant d'obtenir la liste des taureaux (identifiant national, nom, stock total restant de paillettes), le tout trié de façon croissante par stock total restant. 

**Réponse** :
```sql
SELECT idNational, stockRestant, nom, stockRestant
FROM Taureau JOIN Lot
ON Lot.idNationalTaureau = Taureau.idNational
ORDER BY stockRestant
```

## Question 1.5
Rajouter les tables et les relations pour gérer des demandes d’insémination.

**Réponse** :
—> Faire le schéma complet, exemple de relation avec les nouvelles tables 
￼

## Question 2.1
Compléter les scénarios d’erreur du cas d’utilisation « Saisir une tournée » concernant la saisie des kilomètres d’une tournée.

**Réponse** :
- Le kilométrage au compteur en fin de tournée saisi est <= 0, le système en informe l’utilisateur et retourne à l’étape 1.
- Le kilométrage au compteur en fin de tournée saisi est inférieur au kilométrage du début, le système en informe l’utilisateur et retourne à l’étape 1.

## Question 2.2
Compléter la méthode onCreate() de la classe MobiDb.

**Réponse** :
```java
String strReq = "CREATE TABLE histoKm(id INTEGER PRIMARY KEY AUTOINCREMENT, date TEXT, marque TEXT, kmDebut INTEGER, kmFin INTEGER, FOREIGN KEY (idInsemin) REFERENCES inseminateur(id),
FOREIGN KEY (idVehicule) REFERENCES vehicule(id))"; 
db.execSQL(strReq); 
```

## Question 3.1.1
Expliquer le mécanisme qui permet de différencier les deux méthodes de même nom (CATournee) de la classe GestionTournee.

**Réponse** : c’est la surcharge de méthode (orienté objet), qui permet d’avoir plusieurs méthodes de même nom.
Leur signatures permettent de les différencier (et plus précisément leurs types de paramètres d’entrée).

## Question 3.1.2
Écrire la méthode getCoordGPS() de la classe Adherent qui renvoie les coordonnées GPS (latitude et longitude) sous la forme d’une chaîne de caractères.

**Réponse** :
- En Java : 
```java
return latitude + “ “ + longitude;
```
- En pseudo-code :
`renvoyer concat(latitude, “ “, longitude)`

## Question 3.1.3 :
Écrire le code de la méthode getAdherents() de la classe GestionTournee.

**Réponse** :
```java
public ArrayList<Adherent> getAdherents() {
    ArrayList<Adherent> resultat = new ArrayList<Adherent>();
    ArrayList<Visite> visites = laTournee.getLesVisites();
    for (int i = 0 ; i < visites.size() ; i++) {
        Adherent adherent = visites.get(i).getLeAdherent();
        result.add(adherent);
    }
    // alternative
    // for (Visite visite : visites) {
    //     result.add(visite.get(i).getLeAdherent());
    // }
    return resultat;
}
```

## Question 3.1.4 :
Écrire le code de la méthode montantAFacturer() de la classe Visite.

**Réponse** :
```java
public float montantAFacturer(){
    float total = 0;
    for (int i = 0 ; i < lesPrestationsVisite.size() ; i++) {
        PrestationVisite prestation = lesPrestationsVisite.get(i);
        total += prestation.getNombreActes() * prestation.getLeTypePrestation().getPrixForfaitaire();
    }
    return total;
}
```

## Question 3.2 :
Compléter les tests unitaires correspondants à la méthode CATournee() de la classe GestionTournee en respectant le scénario fourni par le chef de projet et en respectant la syntaxe utilisée.

**Réponse** :
```java
@Test
public void testCATournee() {
    // Vérification que le montant total à facturer initial est nul ;
    assertEquals(gt.CATournee(), 0);

    // Vérification du montant total à facturer à l’issue de la première visite ;
    v1.ajouterPrestationVisite(tp1, 3); // ajouter 3 inséminations
    v1.ajouterPrestationVisite(tp2, 1); // ajouter 1 échographie
    assertEquals(gt.CATournee(), 600);

    // Vérification du montant total à facturer final de la tournée.
    v2.ajouterPrestationVisite(tp1, 1);
    assertEquals(gt.CATournee(), 600);
}
```

## Question 4.1
Calculer le ROI du projet

**Réponse** :
Rappel : le ROI = (gain - coût de l’investissement) / coût de l’investissement

Gain estimé \
	= (2 millions * 0,4€/km) - (1,9 millions * 0,4€/km) \
	= 40 000 €

Coût estimé \
	= 6 jours de chef de projet à 600€ + 12 jours de développer à 400€ \
	= 6 * 600€ + 12 * 400€ \
	= 10 000 €

D’où le ROI = (40 000 - 10 000) / 10 000 \
			= 30 000 / 10 000 \
			= 3

Ce qui donne un investissement rentable à 300%
	
(Rappel : 300% = 300 / 100 = 3)

## Question 4.2
Justifier dans une courte note la pertinence de cet investissement, connaissant la durée de rentabilité. 

**Réponse** :
L’investissement est très intéressant connaissant la durée de rentabilité assez courte (92 jours, moins de 3 mois) et la mise initiale de 10 000 € est très faible par rapport au CA généré par l’entreprise (20 millions).

