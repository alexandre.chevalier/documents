2.1.a.
```sql
SELECT nom, prenom, mail FROM Conso
JOIN Vente ON Vente.idConso = id
WHERE YEAR(dateVente) = 2017;
```

2.1.b.
```sql
SELECT COUNT(*) FROM ConsoFidele 
WHERE TIMESTAMPDIFF(YEAR, CURDATE(), dateNaiss) >= 18 
AND TIMESTAMPDIFF(YEAR, CURDATE(), dateNaiss) <= 30
```

2.1.c.
```sql
SELECT Conso.nom, Conso.prenom, Conso.mail,SUM(montantVente) as montant_total
FROM Vente JOIN Conso ON idConso = Conso.idGROUP BY idConso
```

2.2
```java
String requete = "select nom, prenom, tel, mail, count(*) as nbVentes "
			+ "from Conso "
			+ "join Vente on idConso = Conso.id "
			+ "join ConsoFidele on id = Conso.id "
+ "group by nom, prenom, tel, mail "
+ "WHERE ConsoFidele.id IS NULL "
+ “AND dateVente between “ + dateDeb + “ and “ + dateFin 
+ "having count(*) >" + seuilVentes "; 
```

2.3
```java
public void testInitConso() {
    ConsoFidele consoTest = new ConsoFidele("Lifo", "Paul", "lifo.paul@gmail.com", "0600000000",
        new SimpleDateFormat("yyyy-MM-dd").parse("1961-01-03"),
		new SimpleDateFormat("yyyy-MM-dd").parse("2017-01-05")
    ); 
	assertEquals(constTest.getPointsFidelite(), 0);
} 
```

2.4
```java
public void testAddMontant() {
    ConsoFidele consoTest = new ConsoFidele("Lifo", "Paul", "lifo.paul@gmail.com", "0600000000",
        new SimpleDateFormat("yyyy-MM-dd").parse("1961-01-03"),
		new SimpleDateFormat("yyyy-MM-dd").parse("2017-01-05")
    ); 

	// Ajouter 150 euro et vérifier qu’on a 10 points
	consoTest.addFidelite(3, 150); 
	assertEquals("erreur calcul 3ème tampon", 10 ,consoTest.getPointsFidelite()); 
	// Ajouter 250 euro et vérifier qu’on 10 + 20 points
	consoTest.addFidelite(3, 250); 
	assertEquals("erreur calcul 3ème tampon", 30 ,consoTest.getPointsFidelite()); 
	// Ajouter 550 euro et vérifier qu’on 10 + 20 + 50 points
	consoTest.addFidelite(3, 550); 
	assertEquals("erreur calcul 3ème tampon", 80 ,consoTest.getPointsFidelite()); 
} 
```

3.1 - Question : 
Rédiger le commentaire de la méthode statVente de la classe Statistique expliquant ce qu'elle retourne. 

Réponse :
`// Renvoie le pourcentage d’achats journaliers par des consommateurs fidèles`

3.2 -
Réponse :
```java
public int getNbVentes() {
	return lesVentes.size();
}
```

3.3 -
Réponse :
```java
class VenteEcommerce extends Vente { 
  private String adresseLivraison;
  private String optionLivraison;
  public VenteEcommerce(String adresseLivraison, String optionLivraison, Date uneDateVente, Conso unConso, double unMontant) { 
    super(uneDateVente, unConso, unMontant)
    this.adresseLivraison = adresseLivraison;
    this.optionLivraison = optionLivraison;
  }
} 
```

```java
class VenteEcommerce extends Vente { 
  private String adresseLivraison;
  private String optionLivraison;
  public VenteEcommerce() { 
    super(new Date(), new Conso(), 0)
    this.adresseLivraison = "";
    this.optionLivraison = "";
  }
  setOptionLivraison(String optionLivraison) {
    this.optionLivraison = optionLivraison;
  }
  setDate(Date date) {
    this.date = date;
  }
} 
```

3.4 - Question : Compléter le code de la méthode compareLieuVente.

Réponse :
```java
public static double compareLieuVente(ArrayList<ConsoFidele> lesConsos) { 
  double totalEcom = 0; //cumul des montants des ventes ecommerce
  double totalMag = 0; // cumul des montants des ventes en magasin
  //parcours de la liste des consommateurs fidèles 
  for(ConsoFidele cf : lesConsos) {
    ArrayList<Vente> lesVentes = cf.getVentes();
    for(Vente vente : lesVentes) {
      if (vente instanceof VenteEcommerce) {
        totalEcom++;
      } else if (vente instanceof VenteMagasin) {
        totalMag++;
      }
    }
  }
  if (totalEcom == 0 && totalMag == 0) {
    return 1;
  }
  if (totalEcom == 0) { return 1; }
  return totalMag/totalEcom; //calcul de l’indice et retour du résultat
}
```

Question 3.5 : 
Expliquer en quoi la dernière instruction "return totalMag/totalEcom" de la méthode compareLieuVente peut poser problème.

Réponse : Si totalEcom = 0 (ce qui est possible si personne n'achète en e-commerce),
alors le programme va planter car on ne peut pas diviser par zéro.

Question 3.6 :
On souhaite ajouter à la classe Conso une méthode qui retourne, pour le consommateur, la liste des ventes d’un montant supérieur à un montant passé en paramètre.

```java
class Conso {
  private String nom;
  private String prenom;
  private String mail;
  private String tel;
  private ArrayList<Vente> lesVentes ;
  public Conso(String nom, String prenom, String mail, String tel){...}
  public boolean estFidele() { return false; }
  public void addUneVente(Vente uneVente) {...}
  // retourne le nombre de ventes enregistrées dans la collection des ventes du consommateur
  public int getNbVentes() { ... À compléter sur votre copie ...}
  // retourne la collection lesVentes qui contient des instances des
  // classeVenteEcommerce et VenteMagasin
  public ArrayList<Vente> getVentes() {... }

  // Renvoie le nombre de ventes dont le montant > à un paramètre donné
  // Il faut un paramètre montant pour comparer
  // Pour chaque vente, on vérifie sur son montant > montant en paramètre
  //    On récupère la vente
  //    Pour vérifier son montant il faut récupérer le montant de la vente
  public int getNbVentesSupMontant(double montant) {
    int leNombreDeVentes = 0;
    for (int i = 0 ; i < getVentes.size() ; i++) {
      Vente vente = getVentes.get(i)
      if(vente.getMontantVente() > montant) {
        // On incrémente le nombre de ventes
        leNombreDeVentes++;
      }
    }
    return leNombreDeVentes;
  }
}
```


4.3 -> Réponse A car tous les critères sont validés par la solution
Haute disponibilité : 99,9% + assurance
Sécurisation des échanges : chiffrement SSL
Sauvegarde quotidienne : sauvegarde en temps réel
