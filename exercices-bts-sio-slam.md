1. Ecrire une fonction de calcul de TVA : Entrée : (prix, taux) —> Sortie : résultat.  
Ecrire un test unitaire associé (étant donné deux valeurs d’entrée, vérifier la valeur de sortie). 

2. Ecrire une fonction qui renvoie une liste contenant : le premier et le dernier élément d’une liste passée en paramètre.  
Ecrire un test unitaire associé.

3. Concevoir une base de données : schéma relationnel ou requêtes SQL de création.  
Deux tables : *PRODUIT* et *FACTURE* liées entre elles par au moins une relation (clés étrangères).  
Info : cette relation est aussi une table.

4. Ecrire une requête SQL pour récupérer les 3 produits les plus facturés (les plus achetés).  

5. Modifier (si besoin) la conception pour de la base *PRODUIT* et *FACTURE* pour rajouter une gestion des stocks et un historique d’achat pour chaque utilisateur. 

6. Ecrire une méthode `toString()` et un constructeur pour la classe suivante :
```java
class User {
    String firstname; 
    String lastname;
    int age;
    // User(…) { … }
    // String toString() { … }
}
```

7. Modifier cette méthode pour corriger son fonctionnement :  
```java
public void getCityFromUser(User user) {
    if (user != null) return "";
    return utilisateur.getCity();
}
```

8. Compléter ce code pour qu’il s’exécute correctement :  
```java
List<String> names = new ArrayList<String>();
names.add("Bill");
names.add("Bob");
names.add("Bobby");
// TODO: code manquant
assertTrue(names.size() == 4);
```

9. Ecrire une requête SQL pour récupérer la moyenne du prix des produits.

10. Ecrire une requête SQL de création d'une table Utilisateur.  
La table doit avoir au moins deux colonnes : nom et un prénom.

11. Que va afficher ce code ?
```java
List<Integer> list = new ArrayList<Integer>();
list.add(3); 
list.add(30); 
list.add(300); 
list.remove(0);
System.out.println(list.get(0)); // ?
```

12. Ecrire une requête SQL pour récupérer le premier produit acheté (le plus ancien d'après sa date) par l'utilisateur ayant l'id "32".

13. Que fait cette requête SQL ?
```sql
SELECT nom FROM Produit as p
WHERE p.prix < 100;
```

14. Que fait cette requête SQL ?
```sql
SELECT Utilisateur.nom, Utilisateur.id SUM (Facture.prix_total) 
FROM Facture JOIN Utilisateur ON Facture.id_utilisateur = Utilisateur.id
GROUP BY Utilisateur.id;
```

15. Ecrire une méthode qui permet de récupérer les noms des produits dont le prix est supérieur à une certaine valeur.
```java
// Récupérer une liste de produits
List<Produit> produits = API.getProduits();
// Récupérer le nom d'un produit
String nom_produit_1 = produits.get(0).getNom();
// Récupérer le prix d'un produit
int prix_produit_1 = produits.get(0).getPrix();

/**
 * Récupérer la liste des noms des produits
 * dont le prix est supérieur à une certaine valeur passée en paramètre.
 */
public List<String> getListeNomsProduit(int prix, List<Produit> produits) {
    List<String> resultat = new ArrayList<>();
    // TODO: votre code ici
    return resultat;
}

List<String> nomsProduits = getListeNomsProduit(200, produits);
System.out.println(nomsProduits); // Pourrait afficher ["PS5", "Xbox", "Switch", "iPhone", ...]
```
